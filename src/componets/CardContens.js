// material
import { alpha, styled } from '@mui/material/styles';
import { Card, Box, Grid, Container, Typography, CardHeader, CardContent, } from '@mui/material';


import React, { useState, useEffect } from 'react';


export default function CardContens(props) {

  console.log("DEV", props);

  const {value, fontSize} = props;

  const RootStyle = styled(Card)(({ theme }) => ({
    boxShadow: 'none',
    textAlign: 'center',
    padding: theme.spacing(5, 0),
  }));
  
  const IconWrapperStyle = styled('div')(({ theme }) => ({
    margin: 'auto',
    display: 'flex',
    borderRadius: '50%',
    alignItems: 'center',
    width: '100px',
    height: '100px',
    textAlign: 'center',
    justifyContent: 'center',
    marginBottom: theme.spacing(2),
    color: theme.palette.error.dark,
    backgroundImage: `linear-gradient(135deg, ${alpha(theme.palette.error.dark, 0)} 0%, ${alpha(
      theme.palette.error.dark,
      0.24
    )} 100%)`
  }));


  return (
      <Card sx={{ mb:1, height:150}} >
          <CardContent sx={{mt:2}}>
            <IconWrapperStyle>
                <Typography sx={{ opacity: 0.72}}>
                    {value}
                </Typography>
            </IconWrapperStyle>
          </CardContent>
      </Card>
  );
}
