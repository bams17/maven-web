import React from "react";
import Chart from "react-google-charts";

export default function ChartContens(props){
    
    const {value} = props;

      
    return (
        <Chart
            chartType="ColumnChart"
            width="100%"
            height="500px"
            data={value}
        />
    );
}
