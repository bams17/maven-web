import './App.css';
import { useEffect, useRef, useState } from 'react';
import {
  Card,
  CardHeader,
  CardContent,
  Box,
  Button,
  Container,
  Typography,
  TextField,
   Grid
} from '@mui/material';
import ChartContens from './componets/ChartContens.js'
import CardContens from './componets/CardContens.js'
import { connect, subscribe } from "mqtt/dist/mqtt"  
import { fetchToken, onMessageListener } from './firebase';


function App() {

  const [numberPlace, setNumberPlace] = useState();
  const [numberBar, setNumberBar] = useState();
  const [valueBar, setValueBar] = useState(0);
  const [buttonStatus, setButtonStatus] = useState(false)
  const [valuePlace, setValuePlace] = useState()

  const [isTokenFound, setTokenFound] = useState(false);

  const [connectionStatus, setConnectionStatus] = useState(false);

  const [dataMqtt, setDataMqtt] = useState();

  const inputNumber = (e) =>{
    setValuePlace(e.replace(/0/gi, ''))
    const data = placeNumber(e.replace(/0/gi, ''))
    setNumberPlace(data)
  }

  const placeNumber = (num, res = [], factor = 1) => {
    if(num){
       const val = (num % 10) * factor;
       console.log("val", val);
       res.unshift(val);
       return placeNumber(Math.floor(num / 10), res, factor * 10);
    };
    return res;
  }
  const inputNumberBar = (e) => {

    setButtonStatus(true);

    if(valueBar!=0 && buttonStatus==true) {
      let numBer = [["Index","Value", { role: 'style' }]]
        for (let i = 0; i < valueBar; i++) {
          const numRan = parseInt(Math.random() * 100)
          numBer.push([i,numRan, colorBars(numRan)])
        }
      setNumberBar(numBer)
    }
  }

  const inputNumberBarStop = () => {
    setButtonStatus(false);
  }

  const colorBars = (e) => {
    let color = ''
    if(e<=25){
      color = 'green'
    }else if(e<=26 && e <= 50){
      color = 'yellow'
    }else if(e<=51 && e <= 75){
      color = 'red'
    }else if(e<=76 && e <= 100){
      color = 'blue'
    }
    return color
  }
// const configFirebase = async () => {
//   const token = await fetchToken(setTokenFound);
//   const SubscribedFire = await onMessageListener()
//   console.log("TOPIK", SubscribedFire);
// }
useEffect(() => { 

  console.log("JALAN CEK");

  const client = connect("wss://mqtt.gt-trak.com:443", {transports: ['websocket']});

        client.subscribe("esp8266/ATM/STATUS")

        client.on('message', (topic, payload, packet) => {
          const response = payload.toString();
          console.log(response);
          setDataMqtt(response);
          console.log("CEK Bambang");
          // setMessages(messages.concat(payload.toString()));
        });

},[]);


// useEffect(() => { 
//   setTimeout(() => {
//     if(valueBar!=0 && buttonStatus==true){
//       let numBer = [["Index","Value", { role: 'style' }]]
//         for (let i = 0; i < valueBar; i++) {
//           const numRan = parseInt(Math.random() * 100)
//           numBer.push([i,numRan, colorBars(numRan)])
//         }
//     setNumberBar(numBer)
//     }
//   }, 3000);
  
// },[numberBar, valueBar, buttonStatus]);





  return (
    <div className="App">
      {/* <Page title="Maven"> */}
        <Container maxWidth="xl">
          <Grid container spacing={5}>
              <Grid item xs={12} md={6} lg={6}>
                <Typography variant="h5" align="left" sx={{mb:1}}>
                    Aplikasi Pecahan Bilangan
                </Typography>
                  <TextField fullWidth value={valuePlace!=undefined?valuePlace:''} label="masukan nomor" id="fullWidth" onChange={(e)=> inputNumber(e.target.value)} />
                  <Grid container spacing={2} sx={{ mt:1}}>
                        {numberPlace&&numberPlace.map(function(item, i){
                          console.log(item);
                          return <Grid key={i} item xs={3} md={3} lg={3}>
                                    <CardContens  value={item}/>
                                </Grid>
                        })}
                  </Grid>
              </Grid>
              <Grid item xs={12} md={6} lg={6}>
                <Typography variant="h5" align="left" sx={{mb:1}}>
                    Aplikasi Interval Bar Chart
                </Typography>
                  <TextField fullWidth 
                              label="masukan nomor" 
                              id="fullWidth" 
                              onChange={(e)=> setValueBar(e.target.value)} 
                              InputProps={{endAdornment: buttonStatus==true?<Button onClick={ () => inputNumberBarStop() } variant="outlined" color="error">Stop</Button>:<Button onClick={ () => inputNumberBar() } variant="outlined" color="success">Generate</Button>}}/>
                  <Grid container spacing={2} sx={{ mt:1}}>
                    <Grid item xs={12} md={12} lg={12}>
                      {numberBar&&<ChartContens  value={numberBar} />}
                      </Grid> 
                  </Grid>

              </Grid>
          </Grid>
        </Container>
      {/* </Page> */}
    </div>
  );
}

export default App;
