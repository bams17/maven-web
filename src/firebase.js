import { initializeApp } from 'firebase/app';
import { getMessaging, getToken, onMessage } from "firebase/messaging";

var firebaseConfig = {
    apiKey: "AIzaSyAHbeGM_coYT9eE84MgQ-Msmj-VC6VbEno",
    authDomain: "trackingdevice-231a0.firebaseapp.com",
    projectId: "trackingdevice-231a0",
    storageBucket: "trackingdevice-231a0.appspot.com",
    messagingSenderId: "69313850039",
    appId: "1:69313850039:web:a184c0094d107ba28efb32"
};

const firebaseApp = initializeApp(firebaseConfig);

const messaging = getMessaging(firebaseApp);

export const fetchToken = (setTokenFound) => {
    
  return getToken(messaging, {vapidKey: 'BPgCBl1bl_Qa_1VkdrRB2TGexgKC_mj01VZJG6Yr3eIRK6qY-zm8MnY2POMB0OXkecvqKClDrmJ2EgPKlRyq1nY'}).then((currentToken) => {
    if (currentToken) {
      console.log('current token for client: ', currentToken);

      fetch('https://iid.googleapis.com/iid/v1/'+currentToken+'/rel/topics/esp8266ATM', {
            method: 'POST',
            headers: new Headers({
            'Authorization': 'key=AAAAECNtarc:APA91bFH3UVZoyVaXb73-9eQD0fxIudhLZcq3RGS4UnMXmO7arqzzhaYjVhguhtcvECxdENXzJSYZZ3R8mrh7LGEWwJV-anO54SKPnMgLthABrXx-rnzpwhqOT_48DtndkiBsnTkCMPg'
            })
        }).then(response => {
            if (response.status < 200 || response.status >= 400) {
            throw 'Error subscribing to topic: '+response.status + ' - ' + response.text();
            }
            console.log('Subscribed to esp8266/ATM');
        }).catch(error => {
            console.error(error);
        })

      setTokenFound(true);
      // Track the token -> client mapping, by sending to backend server
      // show on the UI that permission is secured
    } else {
      console.log('No registration token available. Request permission to generate one.');
      setTokenFound(false);
      // shows on the UI that permission is required 
    }
  }).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
    // catch error while creating client token
  });
}

export const onMessageListener = () =>
  new Promise((resolve) => {
    onMessage(messaging, (payload) => {
      resolve(payload);
    });
});